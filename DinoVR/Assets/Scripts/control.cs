﻿using UnityEngine;
using System.Collections;

public class control : MonoBehaviour {
	public CharacterController controller;
	public Transform head;
	public bool walking;
	public float speed;
	public float gravity;

	// Use this for initialization
	void Start () {
		controller = GetComponent<CharacterController> ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Cardboard.SDK.Triggered) {
			if (walking) {
				walking = false;
			} else {
				walking = true;
			}
		}

		if (walking) {
			Walk ();
		}

		controller.Move(-transform.up * gravity * Time.deltaTime);
	}

	void Walk() {
		Vector3 moveDirection = head.forward;
		moveDirection.y = 0;
		controller.Move (moveDirection * speed * Time.deltaTime);
	}
}
