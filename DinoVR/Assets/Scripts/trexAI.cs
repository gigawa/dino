﻿using UnityEngine;
using System.Collections;

public class trexAI : MonoBehaviour {
	public string[] prey;

	public float timer;
	public float time;
	public float huntTimer;
	float wanderDistance;
	public float distance;
	float maxDistance;

	float sleepTime;
	float wakeTime;
	float [] waterTime;
	float [] huntTime;
	float [] nestTime;

	public int huntNumber;

	public float stime;

	Transform nest1;
	Transform water1;
	Transform[] hunt1;
	public Transform preyTarget;
	Transform player;

	DinoControl dcontrol;
	Sun sun;

	public bool reachedDestination;
	public bool canMove;
	public bool atNest;
	public bool atWater;
	public bool atHunt;
	public bool active;
	public bool wander;
	public bool sleep;
	public bool hasTarget;
	public bool eat;

	CardboardAudioSource audioS;
	AudioClip footsteps;

	Vector3 target;
	Animator animator;
	NavMeshAgent agent;
	Rigidbody rigid;

	// Use this for initialization
	void Start () {
		agent = GetComponent<NavMeshAgent> ();
		animator = GetComponent<Animator> ();
		audioS = GetComponent<CardboardAudioSource> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		rigid = GetComponent<Rigidbody> ();
		agent.avoidancePriority = Random.Range (0, 100);
		sun = GameObject.FindGameObjectWithTag ("Sun").GetComponent<Sun> ();
		dcontrol = GameObject.FindGameObjectWithTag ("DinoControl").GetComponent<DinoControl> ();
		maxDistance = dcontrol.maxDistance;
		footsteps = dcontrol.footsteps;
		audioS.maxDistance = dcontrol.audioDistance;

		if (gameObject.tag == "Trex") {
			wakeTime = dcontrol.Trex.wake;
			sleepTime = dcontrol.Trex.sleep;
			waterTime = dcontrol.Trex.water;
			huntTime = dcontrol.Trex.hunt;
			nestTime = dcontrol.Trex.nest;
			prey = dcontrol.Trex.prey;
			nest1 = dcontrol.Trex.nest1;
			water1 = dcontrol.Trex.water1;
			hunt1 = dcontrol.Trex.hunt1;
			wanderDistance = dcontrol.Trex.wander;
		}
	}

	// Update is called once per frame
	void Update () {
		distance = Vector3.Distance (this.transform.position, player.position);
		if (distance <= maxDistance) {
			active = true;
		} else {
			active = false;
		}

		if (sun.hour > sleepTime || sun.hour < wakeTime) {
			if (sleep == false) {
				sleep = true;
				animator.SetBool ("Sleep", true);
			}
			agent.Stop ();
		} else if (sun.hour > wakeTime && sun.hour < sleepTime) {
			sleep = false;
			animator.SetBool ("Sleep", false);
			agent.Resume ();
		}

		stime = sun.hour;

		if (!sleep) {
			for (int i = 0; i < nestTime.Length; i++) {
				if (sun.hour >= nestTime [i] && sun.hour <= nestTime [i] + 1 && !atNest) {
					atNest = true;
					atHunt = false;
					atWater = false;

					target = nest1.position + (Random.insideUnitSphere * wanderDistance);
					agent.destination = target;
					timer = 5f;
					canMove = false;
					wander = false;
					hasTarget = false;
					eat = false;
				}
			}

			for (int i = 0; i < huntTime.Length; i++) {
				if (sun.hour >= huntTime [i] && sun.hour <= huntTime [i] + 1 && !atHunt && !eat) {
					if (Random.Range (1, 2) == 1) {
						atNest = false;
						atHunt = true;
						atWater = false;

						huntNumber = Random.Range (0, hunt1.Length);

						target = hunt1 [huntNumber].position + (Random.insideUnitSphere * wanderDistance);
						agent.destination = target;
						timer = 5f;
						canMove = false;
						wander = false;
					} else {
						atNest = true;
						atHunt = false;
						atWater = false;

						target = nest1.position + (Random.insideUnitSphere * wanderDistance);
						agent.destination = target;
						timer = 5f;
						canMove = false;
						wander = false;
						hasTarget = false;
						eat = false;
					}
				}
			}

			for (int i = 0; i < waterTime.Length; i++) {
				if (sun.hour >= waterTime [i] && sun.hour <= waterTime [i] + 1 && !atWater) {
					atNest = false;
					atHunt = false;
					atWater = true;

					target = water1.position + (Random.insideUnitSphere * wanderDistance);
					agent.destination = target;
					timer = 5f;
					canMove = false;
					wander = false;
					hasTarget = false;
					eat = false;
				}
			}

			if (rigid.velocity.sqrMagnitude > 0 || agent.hasPath) {
				if (!audioS.isPlaying) {
					audioS.clip = footsteps;
					audioS.Play ();
				}
				animator.SetBool ("Walk", true);
			} else {
				animator.SetBool ("Walk", false);
				audioS.Pause ();
			}

			if (timer > 0) {
				timer -= Time.deltaTime;
			}

			if (preyTarget == null) {
				hasTarget = false;
			}

			if (hasTarget) {
				agent.destination = preyTarget.position;
				if (Vector3.Distance (this.transform.position, preyTarget.position) < 8) {
					animator.SetTrigger ("Bite");
					if (Random.Range (1, 10) == 1) {
						animator.SetBool ("Eat", true);
						eat = true;
						preyTarget.SendMessage ("Dead");
						hasTarget = false;
					}
				}
			}

			if (agent.remainingDistance == 0 && canMove == false) {

				if (huntTimer > 0) {
					huntTimer -= Time.deltaTime;
				}

				if (timer <= 0) {
					timer = 5f;
					wander = true;
					if (atHunt) {
						if(!hasTarget) {
							preyTarget = FindTarget ();
						}
					}
				} else if(active){
					if (huntTimer <= 0 && wander && !atHunt) {
						agent.destination = (transform.position + (Random.insideUnitSphere * wanderDistance));
						huntTimer = 10f;
					}
				}
			}
		}
	}

	Transform FindTarget() {
		GameObject[] ptarget;
		Transform finalTarget = null;
		float tDistance;
		float closestDistance = Mathf.Infinity;

		for (int i = 0; i < prey.Length; i++) {
			ptarget = GameObject.FindGameObjectsWithTag (prey [i]);

			foreach (GameObject t in ptarget) {
				tDistance = Vector3.Distance (this.transform.position, t.transform.position);
				if (tDistance < closestDistance) {
					closestDistance = tDistance;
					finalTarget = t.transform;
				}
			}
		}

		hasTarget = true;
		return finalTarget;
	}

	void FinishEat() {
		Destroy (preyTarget.gameObject);
		animator.SetBool ("Eat", false);
		target = nest1.position + (Random.insideUnitSphere * wanderDistance);
		agent.destination = target;
		timer = 5f;
		canMove = false;
		wander = false;
		hasTarget = false;
	}
}