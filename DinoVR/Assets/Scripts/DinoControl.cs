﻿using UnityEngine;
using System.Collections;

public class DinoControl : MonoBehaviour {
	[System.Serializable]
	public class dino {
		public string[] pred;
		public float[] nest;
		public float[] graze;
		public float[] water;
		public float sleep;
		public float wake;
		public float wander;
		public Transform nest1;
		public Transform graze1;
		public Transform water1;
	}

	[System.Serializable]
	public class carn {
		public string [] prey;
		public float[] nest;
		public float[] hunt;
		public float[] water;
		public float sleep;
		public float wake;
		public float wander;
		public Transform nest1;
		public Transform [] hunt1;
		public Transform water1;
	}

	public dino Trike;
	public dino Stego;
	public dino Galli;
	public carn Trex;

	public float maxDistance;
	public float predDistance;
	public float audioDistance;

	public AudioClip footsteps;

	// Use this for initialization
	void Start () {
		gameObject.tag = "DinoControl";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
