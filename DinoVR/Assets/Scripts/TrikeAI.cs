﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrikeAI : MonoBehaviour {

	public float timer;
	public float time;
	public float grazeTimer;
	float wanderDistance;
	public float distance;
	float maxDistance;

	float sleepTime;
	float wakeTime;
	float [] waterTime;
	float [] grazeTime;
	float [] nestTime;
	float predDistance;

	public float stime;

	string [] pred;

	public Transform currPred;

	Transform nest1;
	Transform water1;
	Transform graze1;
	Transform player;

	DinoControl dcontrol;
	Sun sun;

	public bool reachedDestination;
	public bool canMove;
	public bool atNest;
	public bool atWater;
	public bool atGraze;
	public bool active;
	public bool wander;
	public bool sleep;
	public bool fleeing;
	bool dead;

	public List <GameObject> predList = new List<GameObject> ();

	CardboardAudioSource audioS;
	AudioClip footsteps;

	Vector3 target;
	Animator animator;
	NavMeshAgent agent;
	Rigidbody rigid;

	// Use this for initialization
	void Start () {
		//timer = Random.Range(0, 10);
		agent = GetComponent<NavMeshAgent> ();
		animator = GetComponent<Animator> ();
		audioS = GetComponent<CardboardAudioSource> ();
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		rigid = GetComponent<Rigidbody> ();
		agent.avoidancePriority = Random.Range (0, 100);
		sun = GameObject.FindGameObjectWithTag ("Sun").GetComponent<Sun> ();
		dcontrol = GameObject.FindGameObjectWithTag ("DinoControl").GetComponent<DinoControl> ();
		maxDistance = dcontrol.maxDistance;
		predDistance = dcontrol.predDistance;
		footsteps = dcontrol.footsteps;
		audioS.maxDistance = dcontrol.audioDistance;

		if (gameObject.tag == "Trike") {
			wakeTime = dcontrol.Trike.wake;
			sleepTime = dcontrol.Trike.sleep;
			waterTime = dcontrol.Trike.water;
			grazeTime = dcontrol.Trike.graze;
			nestTime = dcontrol.Trike.nest;
			nest1 = dcontrol.Trike.nest1;
			water1 = dcontrol.Trike.water1;
			graze1 = dcontrol.Trike.graze1;
			wanderDistance = dcontrol.Trike.wander;
			pred = dcontrol.Trike.pred;
		}

		if (gameObject.tag == "Stego") {
			wakeTime = dcontrol.Stego.wake;
			sleepTime = dcontrol.Stego.sleep;
			waterTime = dcontrol.Stego.water;
			grazeTime = dcontrol.Stego.graze;
			nestTime = dcontrol.Stego.nest;
			nest1 = dcontrol.Stego.nest1;
			water1 = dcontrol.Stego.water1;
			graze1 = dcontrol.Stego.graze1;
			wanderDistance = dcontrol.Stego.wander;
			pred = dcontrol.Stego.pred;
		}

		if (gameObject.tag == "Galli") {
			wakeTime = dcontrol.Galli.wake;
			sleepTime = dcontrol.Galli.sleep;
			waterTime = dcontrol.Galli.water;
			grazeTime = dcontrol.Galli.graze;
			nestTime = dcontrol.Galli.nest;
			nest1 = dcontrol.Galli.nest1;
			water1 = dcontrol.Galli.water1;
			graze1 = dcontrol.Galli.graze1;
			wanderDistance = dcontrol.Galli.wander;
			pred = dcontrol.Galli.pred;
		}

		for (int i = 0; i < pred.Length; i++) {
			GameObject[] p = GameObject.FindGameObjectsWithTag (pred [i]);
			for (int j = 0; j < p.Length; j++) {
				predList.Add (p [j]);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		audioS.maxDistance = dcontrol.audioDistance;
		if (!dead) {
			distance = Vector3.Distance (this.transform.position, player.position);
			if (distance <= maxDistance) {
				active = true;
			} else {
				active = false;
			}

			if (sun.hour > sleepTime || sun.hour < wakeTime) {
				if (sleep == false) {
					sleep = true;
					animator.SetBool ("Sleep", true);
				}
				agent.Stop ();
			} else if (sun.hour > wakeTime && sun.hour < sleepTime) {
				sleep = false;
				animator.SetBool ("Sleep", false);
				agent.Resume ();
				/*atNest = false;
			atGraze = false;
			atWater = false;*/
			}

			stime = sun.hour;

			if (!sleep) {
				/*if (!atNest && canMove) {
				target = nest1.position + (Random.insideUnitSphere * wanderDistance);
				agent.destination = target;
				timer = 5f;
				atNest = true;
				canMove = false;
			} else if (!atGraze && canMove) {
				target = graze1.position + (Random.insideUnitSphere * wanderDistance);
				agent.destination = target;
				timer = 5f;
				atGraze = true;
				canMove = false;
			} else if (!atWater && canMove) {
				target = water1.position + (Random.insideUnitSphere * wanderDistance);
				agent.destination = target;
				timer = 5f;
				atWater = true;
				canMove = false;
			} else if (atNest && atWater && atGraze) {
				atNest = false;
				atWater = false;
				atGraze = false;
			}*/
				for (int i = 0; i < nestTime.Length; i++) {
					if (sun.hour >= nestTime [i] && sun.hour <= nestTime [i] + 1 && !atNest) {
						atNest = true;
						atGraze = false;
						atWater = false;

						target = nest1.position + (Random.insideUnitSphere * wanderDistance * 2);
						agent.destination = target;
						timer = 5f;
						canMove = false;
						wander = false;
					}
				}

				for (int i = 0; i < grazeTime.Length; i++) {
					if (sun.hour >= grazeTime [i] && sun.hour <= grazeTime [i] + 1 && !atGraze) {
						atNest = false;
						atGraze = true;
						atWater = false;

						target = graze1.position + (Random.insideUnitSphere * wanderDistance);
						agent.destination = target;
						timer = 5f;
						canMove = false;
						wander = false;
					}
				}

				for (int i = 0; i < nestTime.Length; i++) {
					if (sun.hour >= waterTime [i] && sun.hour <= waterTime [i] + 1 && !atWater) {
						atNest = false;
						atGraze = false;
						atWater = true;

						target = water1.position + (Random.insideUnitSphere * wanderDistance);
						agent.destination = target;
						timer = 5f;
						canMove = false;
						wander = false;
					}
				}

				if (rigid.velocity.sqrMagnitude > 0 || agent.hasPath) {
					if (!audioS.isPlaying) {
						audioS.Play ();
						audioS.clip = footsteps;
					}
					animator.SetBool ("Walk", true);
					animator.SetBool ("Graze", false);
				} else {
					animator.SetBool ("Walk", false);
					audioS.Pause ();
					if ((atWater || atGraze) || (!atWater && !atGraze && !atNest)) {
						animator.SetBool ("Graze", true);
					}
				}

				if (timer > 0) {
					timer -= Time.deltaTime;
				}

				if (!fleeing) {
					foreach (GameObject p in predList) {
						float d = Vector3.Distance (this.transform.position, p.transform.position);

						if (d < predDistance && !fleeing) {
							currPred = p.transform;
							fleeing = true;
							//Debug.Log (d);
						}
					}
				} else {

					if (Vector3.Distance (this.transform.position, currPred.transform.position) > predDistance) {
						fleeing = false;
					} else {
						agent.destination = nest1.position;//(currPred.transform.position - this.transform.position).normalized + transform.position * wanderDistance;
					}
				}

				if (agent.remainingDistance == 0 && canMove == false) {
					if (grazeTimer > 0) {
						grazeTimer -= Time.deltaTime;
					}
					fleeing = false;
					if (timer <= 0) {
						/*if (reachedDestination) {
						canMove = true;
						reachedDestination = false;
					} else {
						timer = delay;
						reachedDestination = true;
					}*/
						timer = 5f;
						wander = true;
					} else if (active) {
						if (grazeTimer <= 0 && wander) {
							agent.destination = (transform.position + (Random.insideUnitSphere * wanderDistance));
							grazeTimer = 5f;
						}
						//wander = true;
					}
				}
			}
		}
	}

	void Dead() {
		dead = true;
		animator.SetBool ("Dead", true);
	}
}
