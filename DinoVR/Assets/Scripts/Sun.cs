﻿using UnityEngine;
using System.Collections;

public class Sun : MonoBehaviour {
	public float hour;
	public float time;
	public float distance = 500f;
	public float conversion = 0.26179f;
	public float offset;
	public float sunrise = 6f;
	public float sunset = 6f;
	public float transition = 0.1f;
	public float speed = 0.01667f;
	public float intensity;
	public float transitionTimer;
	public Vector3 targetPosition;
	public Vector3 moonPos;
	public Material sky;
	public Material night;
	public Transform moon;
	public bool transitioning;

	// Use this for initialization
	void Start () {
		intensity = 1;
		transition = (speed / 60)/6;
	}

	// Update is called once per frame
	void Update () 
	{
		hour += (speed/60) * Time.deltaTime;
		if (hour > 24) {
			hour -= 24;
		}
		if (hour < sunrise + 3) {
			if (offset < sunrise) {
				offset += transition * Time.deltaTime;
			} else if (offset > sunrise) {
				offset -= transition * Time.deltaTime;
			}
			//RenderSettings.skybox = night;
		} else {
			if (offset < sunset) {
				offset += transition * Time.deltaTime;
			}else if (offset > sunset) {
				offset -= transition * Time.deltaTime;
			}
			//RenderSettings.skybox = sky;
		}
		time = hour - offset;
		targetPosition.Set (0f, distance * Mathf.Sin (conversion * time), distance * Mathf.Cos (conversion * time));
		moonPos.Set (0f, -1 * distance * Mathf.Sin (conversion * time), -1 * distance * Mathf.Cos (conversion * time));
		transform.position = (targetPosition);
		moon.transform.position = (moonPos);
		transform.LookAt(Vector3.zero);
		moon.transform.LookAt(Vector3.zero);
		RenderSettings.ambientIntensity = intensity;

		if ((time < 0 && time > -1 && intensity < 1) || (time > 11 && intensity < 1)) {
			transitionTimer = 5f;
			transitioning = true;
		}

		if (transitioning) {

			if (transitionTimer > 0) {
				transitionTimer -= Time.deltaTime;
			} else {
				transitioning = false;
			}
		}

		if (time > 0 && time < 12) {
			intensity = 1;
			RenderSettings.fogDensity = 0.01f;
		} else {
			intensity = 1;
			RenderSettings.fogDensity = 0.02f;
		}
	}
}
